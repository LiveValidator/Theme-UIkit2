/**
 * The test suite for the uikit2 theme "class" (UIkit2.js)
 */

/* globals uikit2 */
describe( 'UIkit2 theme', function() {
    describe( 'check instantiation', function() {
        uikit2.instantiationSpec();
    } );
    describe( 'check `markRequired` when', function() {
        uikit2.markRequiredSpec();
    } );
    describe( 'check `unmarkRequired` when', function() {
        uikit2.unmarkRequiredSpec();
    } );
    describe( 'check `setMissing` when', function() {
        uikit2.setMissingSpec();
    } );
    describe( 'check `unsetMissing` when', function() {
        uikit2.unsetMissingSpec();
    } );
    describe( 'check `addErrors` when', function() {
        uikit2.addErrorsSpec();
    } );
    describe( 'check `clearErrors` when', function() {
        uikit2.clearErrorsSpec();
    } );
} );
