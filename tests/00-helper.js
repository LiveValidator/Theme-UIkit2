var helper = helper || {};

helper.bareInput = function() {
    setFixtures( '<input />' );
    return document.getElementsByTagName( 'input' )[ 0 ];
};

helper.getRow = function() {
    setFixtures( '<div class="uk-form-row"><label class="uk-form-label">Label</label>' +
    '<div class="uk-form-controls"><input /></div></div>' );

    return document.getElementsByClassName( 'uk-form-row' )[ 0 ];
};
