/**
 * The test suite for the uikit2Tooltip theme "class" (UIkit2Tooltip.js)
 */

/* globals uikit2Tooltip */
describe( 'UIkit2Tooltip theme', function() {
    describe( 'check instantiation', function() {
        uikit2Tooltip.instantiationSpec();
    } );
    describe( 'check `addErrors` when', function() {
        uikit2Tooltip.addErrorsSpec();
    } );
    describe( 'check `clearErrors` when', function() {
        uikit2Tooltip.clearErrorsSpec();
    } );
} );
