/* globals UIkit */

// Get namespace ready
LiveValidator.themes = LiveValidator.themes || {};

LiveValidator.themes.UIkit2Tooltip = function UIkit2TooltipTheme( element, options ) {

    // Scope-safe the object
    if ( !( this instanceof LiveValidator.themes.UIkit2Tooltip ) ) {
        return new LiveValidator.themes.UIkit2Tooltip( element, options );
    }

    // Call parent (UIkit2) constructor
    LiveValidator.themes.UIkit2.call(
        this,
        element,
        LiveValidator.utils.extend(
            {},
            { tooltip: {
                pos: 'bottom-left',
                animation: true
            } },
            options
        )
    );

    this.tooltip = UIkit.tooltip( UIkit.$( this.element ), this.options.tooltip );
};

// Inherit methods from UIkit2
LiveValidator.themes.UIkit2Tooltip.prototype = Object.create( LiveValidator.themes.UIkit2.prototype );
LiveValidator.themes.UIkit2Tooltip.prototype.constructor = LiveValidator.themes.UIkit2Tooltip;

LiveValidator.themes.UIkit2Tooltip.prototype.clearErrors = function() {

    // Change visuals and internals as is needed
    this.unsetMissing();
    this.tooltip.element.data( 'cached-title', '' );

    // Get tooltip to manipulate it
    var $tooltip = $( '.uk-tooltip' );

    // The same hide() code from the tooltip.js file with extras removed.
    if ( this.tooltip.options.animation ) {
        $tooltip.fadeOut( parseInt( this.tooltip.options.animation, 10 ) || 400, function() {
            $tooltip.removeClass( this.tooltip.options.activeClass );
        }.bind( this ) );
    } else {
        $tooltip.hide().removeClass( this.tooltip.options.activeClass );
    }
};
LiveValidator.themes.UIkit2Tooltip.prototype.addErrors = function( errors ) {
    errors = errors.join( '<br>' );

    // Get tooltip to manipulate it
    var $tooltip = $( '.uk-tooltip' );

    // Set errors and change internals
    this.tooltip.element.data( 'cached-title', errors );
    $tooltip.find( '.uk-tooltip-inner' ).html( errors );
    this.setMissing();

    if ( !$tooltip.hasClass( this.tooltip.options.activeClass ) && this.element === document.activeElement ) {
        this.tooltip.show();
    }
};
