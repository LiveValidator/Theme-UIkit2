# LiveValidator - Theme UIkit2

[![build status](https://gitlab.com/LiveValidator/Theme-UIkit2/badges/master/build.svg)](https://gitlab.com/LiveValidator/Theme-UIkit2/commits/master)
[![coverage report](https://gitlab.com/LiveValidator/Theme-UIkit2/badges/master/coverage.svg)](https://gitlab.com/LiveValidator/Theme-UIkit2/commits/master)

UIkit2 themes for LiveValidator. These UIkit2 themes should integrate seamlessly with any project that already uses UIkit2.

Three options exist to show input errors:
- `uk-form-help-block` from the form component
- Tooltips via its component

Find the project [home page and docs](https://livevalidator.gitlab.io/).
